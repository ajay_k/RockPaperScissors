//
//  game.cpp
//  RockPaperScissors
//
//  Created by Ajay Kumar on 3/27/17.
//  Copyright © 2017 NexGen. All rights reserved.
//

#include <iostream>

using namespace std;


int playGame(int playerChoice);
int main(int argc, const char * argv[]) {

    int playerChoice;
    
    do {
        cout << "Press 1 for Rock " << endl;
        cout << "Press 2 for Paper " << endl;
        cout << "Press 3 for Scissors " << endl;
        cout << "Press 0 to EXIT" << endl;
        
        cout << endl << "Please select select your move: " << endl;
        cin >> playerChoice;
        if (playerChoice < 0 || playerChoice > 3) {
            cout << "Invald range, please re-enter range from 1-3 or 3 to EXIT" << endl;
            cout << endl << "Please select select your move: " << endl;
            cin >> playerChoice;
        }
        playGame(playerChoice);
    } while (playerChoice != 0);

    return 0;
}

int playGame(int playerChoice) {
    
    srand(time(NULL));
    int computerChoice;
    computerChoice = (rand() % 3 + 1);
    
    cout << endl;
    switch (computerChoice) {
        case 1:
            cout << "Computer picked Rock" << endl;
            break;
        case 2:
            cout << "Computer picked Paper" << endl;
            break;
        case 3:
            cout << "Computer picked Scissors" << endl;
            break;
            
        default:
            break;
    }
    
    if (computerChoice == 1) {
        
        if (playerChoice == 1) {
            cout << "You picked Rock" << endl;
            cout << "---------------------------->  Tie" << endl;
        }
        if (playerChoice == 2) {
            cout << "You picked Paper" << endl;
            cout << "---------------------------->  Player Wins" << endl;
        }
        if (playerChoice == 3) {
            cout << "You picked Scissors" << endl;
            cout << "---------------------------->  Computer Wins" << endl;
        }
    }
    
    if (computerChoice == 2) {
        
        if (playerChoice == 1) {
            cout << "You picked Rock" << endl;
            cout << "---------------------------->  Computer Wins" << endl;
        }
        if (playerChoice == 2) {
            cout << "You picked Paper" << endl;
            cout << "---------------------------->  Tie" << endl;
        }
        if (playerChoice == 3) {
            cout << "You picked Scissors" << endl;
            cout << "---------------------------->  Player Wins" << endl;
        }
    }
    
    if (computerChoice == 3) {
        
        if (playerChoice == 1) {
            cout << "You picked Rock" << endl;
            cout << "---------------------------->  Player Wins" << endl;
        }
        if (playerChoice == 2) {
            cout << "You picked Paper" << endl;
            cout << "---------------------------->  Computer Wins" << endl;
        }
        if (playerChoice == 3) {
            cout << "You picked Scissors" << endl;
            cout << "---------------------------->  Tie" << endl;
        }
    }
    cout << endl;
    
    return playerChoice;

}
